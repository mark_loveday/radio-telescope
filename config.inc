# Configuration file for TestSensor.py 

# You will need to set user, password and spreadsheet below.
# The sensorDAQ class needs an existing spreadsheet in
# you google Doc
[google]
user = <your gmail address>
password = <your gmail password>
spreadsheet = <your spreadsheet filename>

# %DATE% is a reserved word, which means that 
# the worksheet is named with the current date
[spreadsheet]
worksheet = %DATE%
column_titles = Time, Random
max_rows = 100

# End of Configuration file