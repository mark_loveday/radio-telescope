#!/usr/bin/python
#
# Copyright (C) 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


__author__ = 'maloveday@gmail.com (Mark Andrew Loveday)'


from xml.etree import ElementTree
import gdata.spreadsheet.service
import ConfigParser
import time
import string 


class sensorDAQ(object):

    def __init__(self, config):
        self.configFile = config
        self.gd_client = gdata.spreadsheet.service.SpreadsheetsService()
        self.gd_client.email = ''
        self.gd_client.password = ''
        self.gd_client.source = 'Google Spreadsheet DAQ'
        self.curr_key = ''
        self.curr_wksht_id = ''
        self.list_feed = None
        self.spreadsheet = ''
        self.wsht_name = ''
        self.wsht_rows = 10
        self.wsht_cols = 1
        self.wsht_titles = ['X']
        self.wsht_curr_row = 2
        self.wsht_using_date = False
        
    def dump(self):
        print "curr_key = " + str(self.curr_key)
        print "curr_wksht_id = " + str(self.curr_wksht_id)
        print "list_feed = " + str(self.list_feed)
        print "spreadsheet = " + str(self.spreadsheet)
        print "wsht_name = " + str(self.wsht_name)
        print "wsht_rows = " + str(self.wsht_rows)
        print "wsht_cols = " + str(self.wsht_cols)
        print "wsht_titles = " + str(self.wsht_titles)
        print "wsht_curr_row = " + str(self.wsht_curr_row)
        print "wsht_using_date = " + str(self.wsht_using_date)
        
    def readConfigFile(self):
        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.read(self.configFile)
        self.gd_client.email = config.get("google", "user")
        self.gd_client.password = config.get("google", "password")
        self.gd_client.ProgrammaticLogin()
        self.spreadsheet = config.get("google", "spreadsheet")
        wsht = config.get("spreadsheet", "worksheet")
        if wsht == "%DATE%":
            self.wsht_using_date = True
            # Set worksheet name to date representation dd/mm/yyyy
            self.wsht_name = time.strftime("%x")
        else:
            self.wsht_name = wsht
            
        titles = config.get("spreadsheet", "column_titles")
        self.wsht_titles = titles.split(', ')
        self.wsht_cols = len(self.wsht_titles)
        self.wsht_rows = string.atoi(config.get("spreadsheet", "max_rows"))
        self.wsht_rows += 1
        
    def selectSpreadsheet(self):
        # Select from the list of spreadsheets
        feed = self.gd_client.GetSpreadsheetsFeed()
        spreadsheet_id = -1
        for i, entry in enumerate(feed.entry):
            if entry.title.text == self.spreadsheet:
                spreadsheet_id = i
                
        # If found,set spreadsheet key
        if spreadsheet_id != -1:
            id_parts = feed.entry[spreadsheet_id].id.text.split('/')
            self.curr_key = id_parts[len(id_parts) - 1]
            
        return spreadsheet_id
        
    def createNewWorksheet(self):
        # Create a new worksheet
        self.gd_client.AddWorksheet(str(self.wsht_name), self.wsht_rows, self.wsht_cols, self.curr_key)
        print "[INFO] Created worksheet: " + str(self.wsht_name)
        
    def selectWorksheet(self):
        # Select from the list of worksheets
        feed = self.gd_client.GetWorksheetsFeed(self.curr_key)
        worksheet_id = -1
        for i, entry in enumerate(feed.entry):
            if entry.title.text == self.wsht_name:
                worksheet_id = i
        
        if worksheet_id != -1:
            id_parts = feed.entry[worksheet_id].id.text.split('/')
            self.curr_wksht_id = id_parts[len(id_parts) - 1]
            
        return worksheet_id
    
    def dumpData(self):
        # Get the feed of cells
        feed = self.gd_client.GetCellsFeed(self.curr_key, self.curr_wksht_id)
        self.__PrintFeed(feed)
            
    def setWorksheetTitles(self):
        # Loop over assigned titles
        for i in range(len(self.wsht_titles)):
            retCode = self.__UpdateData(1, (i+1), str(self.wsht_titles[i]))
            if retCode:
                print "[INFO] Worksheet title " + str(self.wsht_titles[i]) + " updated"
            else:
                print "[ERROR] Set worksheet title request failed"
            
    def setData(self, inputArray):
        # If using date option
        if self.wsht_using_date:
            # If current worksheet is not equal current date
            if self.wsht_name != time.strftime("%x"):
                # Create new worksheet and reset row count
                self.wsht_name = time.strftime("%x")
                self.createNewWorksheet()
                self.selectWorksheet()
                self.setWorksheetTitles()
                self.wsht_curr_row = 2
        
        # Return, if trying to write beyond worksheet rows
        if self.wsht_curr_row > self.wsht_rows:
            print "[ERROR] Cannot write out of bounds"
            return False
                
        # Loop over input Array
        for i in range(len(inputArray)):
            retCode = self.__UpdateData(self.wsht_curr_row, (i+1), str(inputArray[i]))
            if retCode:
                print "[INFO] column " + self.wsht_titles[i] + " updated"
            else:
                print "[ERROR] Set data for " + self.wsht_titles[i] + " failed"
        
        self.wsht_curr_row += 1
        return True
        
    def __UpdateData(self, row, col, inputValue):
        retCode = False
        entry = self.gd_client.UpdateCell(row=row, col=col, inputValue=inputValue, 
                                          key=self.curr_key, wksht_id=self.curr_wksht_id)
        if isinstance(entry, gdata.spreadsheet.SpreadsheetsCell):
            retCode = True
            
        return retCode
            
    def __PrintFeed(self, feed):
        for i, entry in enumerate(feed.entry):
            if isinstance(feed, gdata.spreadsheet.SpreadsheetsCellsFeed):
                print '%s %s\n' % (entry.title.text, entry.content.text)
            elif isinstance(feed, gdata.spreadsheet.SpreadsheetsListFeed):
                print '%s %s %s' % (i, entry.title.text, entry.content.text)
                # Print this row's value for each column (the custom dictionary is
                # built using the gsx: elements in the entry.)
                print 'Contents:'
                for key in entry.custom:  
                    print '  %s: %s' % (key, entry.custom[key].text) 
                    print '\n',
            else:
                print '%s %s\n' % (i, entry.title.text)
        
