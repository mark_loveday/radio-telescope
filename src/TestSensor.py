#!/usr/bin/python
#
# Copyright (C) 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


__author__ = 'maloveday@gmail.com (Mark Andrew Loveday)'

from sensorDAQ import sensorDAQ
from random import randrange
import getopt
import sys
import time


def main():
    # parse command line options
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["config="])
    except getopt.error, msg:
        print '[ERROR] python TestSensor.py --config [config file]'
        sys.exit(2)

    config = ''

    # Process options
    for o, a in opts:
        if o == "--config":
            config = a

    if config == '':
        print '[ERROR] python TestSensor.py --config [config file]'
        sys.exit(2)

    mySensor = sensorDAQ(config)
    mySensor.readConfigFile()
    retCode = mySensor.selectSpreadsheet()

    if retCode == -1:
        print "[ERROR] configured spreadsheet not found."

    mySensor.createNewWorksheet()
    retCode = mySensor.selectWorksheet()

    if retCode == -1:
        print "[ERROR] configured worksheet not found."

    mySensor.setWorksheetTitles()

    flag = True
    while(flag):
        data = []
        # Get current time stamp
        data.append(time.strftime("%X"))
        # Generate random number between 1 and 100
        data.append(randrange(1,101))
        flag = mySensor.setData(data)

    mySensor.dumpData()

if __name__ == '__main__':
    main()

