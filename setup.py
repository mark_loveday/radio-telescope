#!/usr/bin/python
#
# Copyright (C) 2013 Mark Loveday
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

import sys
from distutils.core import setup

required = []

setup(
    name='radio-telescope',
    version='0.0.1',
    description='Writes data to Google Spreadsheet',
    long_description="""
This module can write to an existing Google Spreadsheet, with a configuration file
to create on the fly a new worksheet with user defined column titles. The data is
then can be written in a loop from an array that has the same number of element
as column titles. 
""",
    author='Mark Loveday',
    author_email='maloveday@gmail.com',
    license='Apache 2.0',
    url='https://bitbucket.org/mark_loveday/radio-telescope',
    packages=[
      'sensorDAQ',
      ],
    package_dir = {'sensorDAQ':'src/sensorDAQ'},
    install_requires=required
)

